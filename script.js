let jsonArray = {
    "response": 200,
    "data": [{
            "key": "1",
            "name": "John Brown",
            "age": 32,
            "address": "New York No. 1 Lake Park, New York No. 1 Lake Park",
            "tags": [
                "nice",
                "developer"
            ]
        },
        {
            "key": "2",
            "name": "Jim Green",
            "age": 42,
            "address": "London No. 2 Lake Park, London No. 2 Lake Park",
            "tags": [
                "loser"
            ]
        },
        {
            "key": "3",
            "name": "Joe Black",
            "age": 32,
            "address": "Sidney No. 1 Lake Park, Sidney No. 1 Lake Park",
            "tags": [
                "cool",
                "teacher"
            ]
        },
        {
            "key": "4",
            "name": "John Doe",
            "age": 32,
            "address": "Sydney No. 1 Lake Park, Sydney No. 1 Lake Park",
            "tags": [
                "awesome",
                "doctor"
            ]
        },
        {
            "key": "6",
            "name": "ra's al ghul",
            "age": 32,
            "address": "Sydney No. 1 Lake Park, Sydney No. 1 Lake Park",
            "tags": [
                "super",
                "villain"
            ]
        }
    ]
}

let myArray = jsonArray.data;

//console.log(myArray);

$('#search-input').on('keyup', function() {

    var value = $(this).val();
    console.log(value);
    var data = searchTable(value, myArray);
    myTable(data);


})


function searchTable(value, data) {



    let filterdata = [];
    for (var i = 0; i < data.length; i++) {


        value = value.toLowerCase();
        var name = data[i].name.toLowerCase();

        if (name.includes(value)) {
            filterdata.push(data[i]);

        }
    }
    return filterdata;
}


const a = myTable(myArray);


function myTable(data) {
    let table = document.getElementById('mytable1');

    table.innerHTML = ""

    //console.log(data);


    for (let i = 0; i < data.length; i++) {

        const key_value = data[i].key;
        const name_value = data[i].name;
        const age_value = data[i].age;
        const address_value = data[i].address;
        const tags_value = data[i].tags;


        let row = `<tr>
                 <td> ${key_value} </td>
                 <td> ${name_value} </td>
                 <td> ${age_value} </td>
                 <td> ${address_value}</td>
                 <td> ${tags_value} </td>
     
            </tr>`

        table.innerHTML += row;



    }

}